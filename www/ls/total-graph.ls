ig.drawYearly = (lastDatum) ->
  container = ig.containers['total']
  return unless container
  container = d3.select container
  class Year
    (@year, @value, @special) ->
  years =
    new Year 2016, 5261
    new Year 2015, 8563
    new Year 2014, 4822
    new Year 2013, 4153
    new Year 2012, 3595
    new Year 2011, 3360
    new Year 2010, 2988
    new Year 2009, 4457
    new Year 2008, 3829
    new Year 2007, 8096
    new Year 2006, 11488
    new Year 2005, 15489
    new Year 2004, 27391
  width = 600

  xScale = ->
    year = it.year || it
    (year - 2004) / (2015 - 2003) * 100

  yScale = d3.scale.linear!
    ..domain [0 27391]
    ..range [0 100]

  num = ig.utils.formatNumber
  maxStep = 2

  historyContainer = d3.select ig.containers['total-history']
    ..append \div
      ..attr \class \bars
      ..selectAll \.bar-container .data years .enter!append \div
        ..attr \class (d, i) -> "bar-container step step-#{Math.min maxStep, i}"
        ..style \left -> "#{xScale ...}%"
        ..style \width "#{xScale 2005}%"
        ..attr \data-year (.year)
        ..append \div
          ..attr \class \bar
          ..style \height -> "#{yScale it.value}%"
          ..append \div
            ..attr \class \label-y
            ..html -> num it.value
        ..append \div
          ..attr \class \label-x
          ..html ->
            o = it.year
            if it.year is 2008 then o += "<span class='asterisk'>*</span>"
            o

  targetOffset = null
  step = ->
    container.selectAll ".step"
      ..transition!
        ..delay (d, i) -> i * 100
        ..attr \class "bar-container step active"
  calcOffset = ->
    {top} = ig.utils.offset container.node!
    targetOffset := top + container.node!offsetHeight / 2

  calcOffset!
  setInterval calcOffset, 1000
  animated = no
  window.addEventListener \scroll ->
    top = (document.body.scrollTop || document.documentElement.scrollTop) + window.innerHeight || 0
    if top > targetOffset and not animated
      animated := yes
      step!
